(function(){
	if (!window.RPEngine){
		window.RPEngine = {};
	}
	window.RPEngine.GraphicsEngine = function(el, config){
		this.physicsEngine = null;
		this.el = $(el);
		this.field = null;
		this.objects = [];
		this.updateInProgress = false;
		this.lastUpdateTime = Date.now();
		this.currentUpdateTime = Date.now();
		this.camera = null;
		this.config = $.extend({}, window.RPEngine.GraphicsEngine.DEFAULT_CONFIG, config);
		this.setPhysicsEngine = function(physicsEngine){
			this.physicsEngine = physicsEngine;
		}
		this.startGameCycle = function(){
			//ToDo fix start/stop/pause
			this.update();
		};
		this.stopGameCycle = function(){
			//ToDo fix start/stop/pause
		};
		this.init = function(){
			this.camera = new window.RPEngine.GraphicsEngine.Camera();
			this.field = $("<div id='field'></div>");
			this.camera.init(this.el, this.field, this.config.CAMERA.SIZE.X, this.config.CAMERA.SIZE.Y);
			this.field.css("width", this.config.CELL_SIZE * this.config.GRID_SIZE.X);
			this.field.css("height", this.config.CELL_SIZE * this.config.GRID_SIZE.Y);
		};
		this.redrawScreen = function(){
			this.update();
		};
		ttt = 0;						//debug
		mmm = 0;						//debug
		this.update = function(){
			//ToDo fix start/stop/pause 
			requestAnimationFrame(this.update.bind(this));
			//setTimeout(this.update.bind(this));
			this.currentUpdateTime = Date.now();
			ttt++;						//debug
			$("#debug").html(ttt);		//debug
			if (this.updateInProgress == false){
				this.updateInProgress = true;
				
				this.processObjectsList(this.objects);
				this.camera.processCamera();
				
				this.updateInProgress = false;
			} else {
				mmm++;					//debug
				$("#debug2").html(mmm);	//debug
			}
			this.lastUpdateTime = this.currentUpdateTime;
		};
		this.processObjectsList = function(objects){
			for (var i = 0; i < objects.length; i++){
				//if (true){
				if (this.camera.rectangle.hasIntersectionWith(objects[i].rectangle)){
					this.processObject(objects[i]);
					objects[i].onCamera = true;
				} else {
					this.hideObject(objects[i]);
					objects[i].onCamera = false;
				}
			}
		};
		this.hideObject = function(obj){
			if (obj.element != null){
				obj.element.remove();
				obj.element = null;
			}
		};
		this.redrawObject = function(obj){
			if (obj.element == null){
				this.initDOMElement(obj);
			}
			obj.element.css("margin-left", obj.rectangle.position.x);
			obj.element.css("margin-top", obj.rectangle.position.y);
			obj.element.css("z-index", obj.rectangle.position.y);
			if (obj.visible){
				obj.element.css("display", "block");
			} else {
				obj.element.css("display", "none");
			}
			obj.updated = false;
			
		};
		this.initDOMElement = function(obj){
			
			obj.element = $("<img></img>");
			obj.element.addClass('game-object');
			obj.element.gameObject = obj;
			obj.element.css("width", obj.rectangle.size.x + "px");
			obj.element.css("height", obj.rectangle.size.y + "px");
			obj.element.css("z-index", obj.rectangle.position.y);
			obj.element.attr("src", Images[obj.type]);
			obj.element.addClass(obj.type);
			this.field.append(obj.element);
			
		};
		this.processObject = function(obj){
			if (obj.dynamic == true){
				var endPoint = obj.rectangle.position;
				if (obj.movementController != null){
					endPoint = obj.movementController.onUpdate(this.currentUpdateTime - this.lastUpdateTime);
				}
				if (!endPoint.equals(obj.rectangle.position) && obj.body != null){
//					if (!this.physicsEngine.detectCollision(obj, endPoint)){
//						obj.position = endPoint;
//					}
					var touchPoint = this.physicsEngine.detectTouchPoint(obj, endPoint);
					if (!touchPoint.equals(endPoint)){
						endPoint = touchPoint;
						if (obj.onCollision){
							obj.onCollision();
						}
					}
				}
				obj.rectangle.position = endPoint;
				this.redrawObject(obj);
			} else {
				if (obj.updated == true || obj.element == null){
					this.redrawObject(obj);
				}
			}
		};
		this.addObject = function(obj){
			obj.setUpdated = true;
			this.initDOMElement(obj);
			this.objects.push(obj);
			if (this.physicsEngine != null && obj.body != null){
				this.physicsEngine.addObject(obj);
			}
		};
		this.removeObject = function(obj){
			console.log("NOT SUPPORTED : this.removeObject(obj)");
		};
	};
	window.RPEngine.GraphicsEngine.Camera = function(){
		this.el = null;
		this.fieldEL = null;
		this.width = null;
		this.height = null;
		this.lastObjPosition = new window.RPEngine.Point(0, 0);
		this.rectangle = null;
		this.obj = null;
		
		this.init = function(parentEl, fieldEl, width, height){
			this.rectangle = new RPEngine.Rectangle(0, 0, width, height)
			this.el = $("<div class='camera'></div>");
			this.fieldEL = fieldEl;
			parentEl.append(this.el);
			this.el.append(this.fieldEL);
		}
		this.processCamera = function(){
			if (!this.lastObjPosition.equals(this.obj.rectangle.position)){
				this.lastObjPosition = this.obj.rectangle.position;
				this.rectangle.position = new RPEngine.Point(
						this.obj.rectangle.position.x - this.rectangle.size.x / 2, this.obj.rectangle.position.y - this.rectangle.size.y / 2);			
				this.redraw();
			}
		}
		this.fixCameraToObj = function(obj){
			this.obj = obj;
		}
		this.redraw = function(){
			this.el.css("width", this.rectangle.size.x);
			this.el.css("height", this.rectangle.size.y);
			this.fieldEL.css("left", - this.rectangle.position.x + "px");
			this.fieldEL.css("top", - this.rectangle.position.y + "px");
		}
	}
	window.RPEngine.GraphicsEngine.DEFAULT_CONFIG = {
		GRID_SIZE: {
			X : 80,
			Y: 80
		},
		CELL_SIZE: 64,
		FPS: 0,
		CAMERA: {
			SIZE: {
				X: 800,
				Y: 400
			}
		}
	}
})();