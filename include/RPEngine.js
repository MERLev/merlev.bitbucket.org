(function(){
	if (!window.RPEngine){
		window.RPEngine = {};
	}
	window.RPEngine.Point = function(x, y){
		this.x = x;
		this.y = y;
		this.equals = function(obj){
			if (obj != null){
				if (obj.x == this.x && obj.y == this.y){
					return true;
				}
			}
			return false;
		};
		this.clone = function(){
			return new window.RPEngine.Point(this.x, this.y);
		};
	};
	window.RPEngine.Rectangle = function(x, y, sizeX, sizeY){
		this.position = new window.RPEngine.Point(x, y);
		this.size = new window.RPEngine.Point(sizeX, sizeY);
		this.equals = function(obj){
			if (obj != null){
				if (this.position.equals(obj.position) && this.size.equals(obj.size)){
					return true;
				}
			}
			return false;
		};
		this.clone = function(){
			return new window.RPEngine.Rectangle(this.position.x, this.position.y, this.size.x, this.size.y);
		};
		this.contains = function(point){
			if (	point.x >= this.position.x && point.x < this.position.x + this.size.x &&
					point.y >= this.position.y && point.y < this.position.y + this.size.y){
				return true;	
			}
		};
		this.hasIntersectionWith = function(rect){
//			if (	this.position.x > rect.position.x + rect.size.x || 
//					this.position.x + this.size.x < rect.position.x || 
//					this.position.y > rect.position.y + rect.size.y || 
//					this.position.y + this.size.y < rect.position.y){ 
			if (	this.position.x < rect.position.x + rect.size.x &&
					this.position.x + this.size.x > rect.position.x && 
					this.position.y < rect.position.y + rect.size.y && 
					this.position.y + this.size.y > rect.position.y){ 
				return true;
			}
			return false;
		}
		this.getOppositePoint = function(){
			return new window.RPEngine.Point(this.position.x + this.size.x, this.position.y + this.size.y);
		}
	}
	window.RPEngine.Movement = {
		controllers: {}
	};
	window.RPEngine.Movement.controllers.DefaultMovementController = function(){
		this.obj = null;
		this.paused = false;
		this.init = function(obj){
			this.obj = obj;
		};
		this.onUpdate = function(){
			console.log("UNSUPPORTED");
		};
		this.onCollision = function(obj){
			console.log("UNSUPPORTED");
		};
		this.claculateLineMove = function(startPoint, angle, speed, time){
			if (speed == 0){
				return this.obj.rectangle.position;
			}
			return new RPEngine.Point(
				Math.round(this.obj.rectangle.position.x + speed * time * Math.sin(angle) / 100),
				Math.round(this.obj.rectangle.position.y - speed * time * Math.cos(angle) / 100)
			);
		};
		this.pause = function(time){
			this.paused = true;
			if (time != null){
				this.delay = time;
			}
		}
		this.resume = function(){
			this.paused = false;
			this.delay = null;
		}
		
	}
	window.RPEngine.Movement.controllers.KeyboardMovementController = function(){
		$.extend(this, new window.RPEngine.Movement.controllers.DefaultMovementController());
		this.obj = null;
		this.KEYS = {
			UP: 38,
			DOWN: 40,
			LEFT: 37,
			RIGHT: 39
		}
		this.keyStatuses = {};
		this.keyStatuses[this.KEYS.UP] = {pressed: false};
		this.keyStatuses[this.KEYS.DOWN] = {pressed: false};
		this.keyStatuses[this.KEYS.LEFT] = {pressed: false};
		this.keyStatuses[this.KEYS.RIGHT] = {pressed: false};
		this.init = function(obj){
			this.obj = obj;
			var th = this;
			$(document).keydown(function(e) {
				if (e.which == th.KEYS.LEFT || e.which == th.KEYS.RIGHT || e.which == th.KEYS.UP || e.which == th.KEYS.DOWN){
					th.keyStatuses[e.which].pressed = true;
					th.calculateAngle();
					e.preventDefault();
				}
			});
			$(document).keyup(function(e) {
				if (e.which == th.KEYS.LEFT || e.which == th.KEYS.RIGHT || e.which == th.KEYS.UP || e.which == th.KEYS.DOWN){
					th.keyStatuses[e.which].pressed = false;
					th.calculateAngle();
					e.preventDefault();
				}
			});
			this.calculateAngle();
		};
		this.calculateAngle = function(){
			this.obj.updated = true;
			this.speed = this.obj.speed;
			if (this.keyStatuses[this.KEYS.UP].pressed && this.keyStatuses[this.KEYS.RIGHT].pressed){
				this.angle = Math.PI/4;
			} else if (this.keyStatuses[this.KEYS.RIGHT].pressed && this.keyStatuses[this.KEYS.DOWN].pressed){
				this.angle = 3*Math.PI/4;
			} else if (this.keyStatuses[this.KEYS.DOWN].pressed && this.keyStatuses[this.KEYS.LEFT].pressed){
				this.angle = 5*Math.PI/4;
			} else if (this.keyStatuses[this.KEYS.LEFT].pressed && this.keyStatuses[this.KEYS.UP].pressed){
				this.angle = 7*Math.PI/4;
			} else if (this.keyStatuses[this.KEYS.UP].pressed){
				this.angle = 0;
			} else if (this.keyStatuses[this.KEYS.RIGHT].pressed){
				this.angle = Math.PI / 2;
			} else if (this.keyStatuses[this.KEYS.DOWN].pressed){
				this.angle = Math.PI;
			} else if (this.keyStatuses[this.KEYS.LEFT].pressed){
				this.angle = 3 * Math.PI / 2;
			} else {
				this.speed = 0;
				this.angle = 0;
			}
		};
		this.onUpdate = function(time){
			return this.claculateLineMove(this.obj.rectangle.position, this.angle, this.speed, time);
		};
		this.onCollision = function(obj){
			console.log("UNSUPPORTED");
		};
	};
	window.RPEngine.Movement.controllers.RandomMovementController = function(){
		$.extend(this, new window.RPEngine.Movement.controllers.DefaultMovementController());
		this.obj = null;
		this.angle = 0;
		var timePassed = 0;
		this.directionChangeTime = 1000;
		this.init = function(obj, directionChangeTime){
			this.obj = obj;
			this.directionChangeTime = directionChangeTime;
		};
		this.onUpdate = function(time){
			//console.log(time);
			if (this.paused){
				if (this.delay != null){
					this.delay -= time;
					if (this.delay < 0 ){
						this.delay = null;
						this.paused = false;
					}
				}
			}
			if (!this.paused){
				timePassed += time;
				if (timePassed >= this.directionChangeTime){
					timePassed = Math.round(timePassed % this.directionChangeTime);
					this.angle = Math.random() * 8 * Math.PI / 4;
					//this.angle += Math.PI / 4;
				}
				return this.claculateLineMove(this.obj.rectangle.position, this.angle, this.obj.speed, time);
			}
			return this.obj.rectangle.position;			
		};
		this.reverseAngle = function(){
			this.angle += Math.PI;	
		}
		this.onCollision = function(obj){
			console.log("UNSUPPORTED");
		};
	}
})();