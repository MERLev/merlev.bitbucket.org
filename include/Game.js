$(document).ready(function(){
	var engine = new RPEngine.GraphicsEngine("#gameScreen", {});
	engine.setPhysicsEngine(new RPEngine.PhysicsEngine());
	engine.init();
	
	var DRAGONS_COUNT = 200;
	var TREES_COUNT = 700;
	
	someObj = new GameObject();
	someObj.setVisible(true);
	someObj.type = "human";
	someObj.speed = 20;
	someObj.rectangle = new RPEngine.Rectangle(700, 500, 32, 32);
	someObj.body = new RPEngine.PhysicsEngine.RectangletBody();
	someObj.body.layers = ['MAIN'];
	someObj.dynamic = true;
	someObj.movementController = new RPEngine.Movement.controllers.KeyboardMovementController();
	someObj.movementController.init(someObj);
	engine.addObject(someObj);
	engine.camera.fixCameraToObj(someObj);
	
		for (var i = 0; i < DRAGONS_COUNT; i++){
			var someObj = new GameObject();
			someObj.setVisible(true);
			someObj.type = "dragon";
			someObj.speed = 8;
			someObj.rectangle = new RPEngine.Rectangle(Math.round((31 * Math.random()) * 80), Math.round((31 * Math.random()) * 80), 
					32, 32);
			someObj.body = new RPEngine.PhysicsEngine.RectangletBody();
			someObj.body.layers = ['MAIN'];
			someObj.dynamic = true;
			someObj.movementController = new RPEngine.Movement.controllers.RandomMovementController();
			someObj.movementController.init(someObj, 1000);
			engine.addObject(someObj);
			someObj.onCollision = function(obj){
				//this.movementController.pause(500);
				this.movementController.reverseAngle();
			}
		}
		
		for (var i = 0; i < TREES_COUNT; i++){
			var obj = new GameObject();
			obj.setVisible(true);
			obj.type = "tree";
			obj.body = new RPEngine.PhysicsEngine.RectangletBody();
			obj.body.layers = ['MAIN'];
			obj.rectangle = new RPEngine.Rectangle(Math.round((31 * Math.random()) * 80), Math.round((31 * Math.random()) * 80), 
					RPEngine.GraphicsEngine.DEFAULT_CONFIG.CELL_SIZE, RPEngine.GraphicsEngine.DEFAULT_CONFIG.CELL_SIZE);
			obj.debug = true;
			engine.addObject(obj);
		}
	/*
	for (var i = 0; i < RPEngine.GraphicsEngine.DEFAULT_CONFIG.GRID_SIZE.X; i++){
		var obj = new GameObject();
		obj.setVisible(true);
		obj.type = "tree";
		//obj.body = new RPEngine.PhysicsEngine.RectangletBody();
		//obj.body.layers = ['MAIN'];
		obj.rectangle = new RPEngine.Rectangle(i * RPEngine.GraphicsEngine.DEFAULT_CONFIG.CELL_SIZE, 0, 
				RPEngine.GraphicsEngine.DEFAULT_CONFIG.CELL_SIZE, RPEngine.GraphicsEngine.DEFAULT_CONFIG.CELL_SIZE);
		engine.addObject(obj);
		
		var obj = new GameObject();
		obj.setVisible(true);
		obj.type = "tree";
		//obj.body = new RPEngine.PhysicsEngine.RectangletBody();
		//obj.body.layers = ['MAIN'];
		obj.rectangle = new RPEngine.Rectangle(i * RPEngine.GraphicsEngine.DEFAULT_CONFIG.CELL_SIZE, (RPEngine.GraphicsEngine.DEFAULT_CONFIG.GRID_SIZE.Y - 1) * RPEngine.GraphicsEngine.DEFAULT_CONFIG.CELL_SIZE, 
				RPEngine.GraphicsEngine.DEFAULT_CONFIG.CELL_SIZE, RPEngine.GraphicsEngine.DEFAULT_CONFIG.CELL_SIZE);
		engine.addObject(obj);
	}
	for (var i = 1; i < RPEngine.GraphicsEngine.DEFAULT_CONFIG.GRID_SIZE.Y - 1; i++){
		var obj = new GameObject();
		obj.setVisible(true);
		obj.type = "tree";
		//obj.body = new RPEngine.PhysicsEngine.RectangletBody();
		//obj.body.layers = ['MAIN'];
		obj.rectangle = new RPEngine.Rectangle(0, i * RPEngine.GraphicsEngine.DEFAULT_CONFIG.CELL_SIZE, 
				RPEngine.GraphicsEngine.DEFAULT_CONFIG.CELL_SIZE, RPEngine.GraphicsEngine.DEFAULT_CONFIG.CELL_SIZE);
		engine.addObject(obj);
		
		var obj = new GameObject();
		obj.setVisible(true);
		obj.type = "tree";
		//obj.body = new RPEngine.PhysicsEngine.RectangletBody();
		//obj.body.layers = ['MAIN'];
		obj.rectangle = new RPEngine.Rectangle((RPEngine.GraphicsEngine.DEFAULT_CONFIG.GRID_SIZE.X - 1) * RPEngine.GraphicsEngine.DEFAULT_CONFIG.CELL_SIZE, i * RPEngine.GraphicsEngine.DEFAULT_CONFIG.CELL_SIZE, 
				RPEngine.GraphicsEngine.DEFAULT_CONFIG.CELL_SIZE, RPEngine.GraphicsEngine.DEFAULT_CONFIG.CELL_SIZE);
		engine.addObject(obj);
	}
	*/
	engine.startGameCycle();
	
	
	ddd = 0;
	setInterval(function(){
		$("#debug3").html(ttt - ddd);
		ddd = ttt;
	}, 1000);
});