var GameObjectClass = {
	TREE: 'tree',
	HUMAN: 'human',
}
var BodyType = {
	SQUARE: 0
}
var Body = function(sizeX, sizeY){
	this.type = BodyType.SQUARE;
	this.sizeX = sizeX;
	this.sizeY = sizeY;
};
var GameObject = function(){
	this.visible = false;
	this.element = null;
	this.body = null;
	this.dynamic = false;
	this.updated = false;
	this.rectangle = new window.RPEngine.Rectangle(0, 0, 0, 0);
	this.speed = 0;
	this.angle = 0;
	this.setRectangle = function(rect){
		this.rectangle = rect;
		this.updated = true;
	}
	this.setPosition = function(position){
		this.rectangle.position = position;
		this.updated = true;
	};
	this.setSize = function(size){
		this.rectangle.size = size;
		this.updated = true;
	}
	this.setVisible = function(visible){
		this.visible = true;
		this.updated = true;
	};
	this.addBody = function(body){
		this.body = body;
	}
}
var Images = {
	human: "include/human.png",
	tree: "include/tree.png", 
	dragon: "include/dragon.png"
}
var Debug= {};
Debug.countOnScreenObjects = function(objs){
	var counter = 0;
	for (var i = 0; i < objs.length; i++){
		if (objs[i].onCamera){
			counter++;
		}
	}
	return counter;
};