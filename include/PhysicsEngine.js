var cal = null;
(function(){
	if (!window.RPEngine){
		window.RPEngine = {};
	}
	window.RPEngine.PhysicsEngine = function(config){
		
		this.config = $.extend({}, window.RPEngine.PhysicsEngine.DEFAULT_CONFIG, config);
		
		// dynamic objects here
		this.dynamicObjects = [];
		
		// static objects here, divided by chunks
		
		this.chunksHolder = new window.RPEngine.PhysicsEngine.ChunksHolder(
				RPEngine.GraphicsEngine.DEFAULT_CONFIG.GRID_SIZE.X * RPEngine.GraphicsEngine.DEFAULT_CONFIG.CELL_SIZE,
				RPEngine.GraphicsEngine.DEFAULT_CONFIG.GRID_SIZE.Y * RPEngine.GraphicsEngine.DEFAULT_CONFIG.CELL_SIZE, 
				this.config.CHUNK_SIZE);
				
		
		this.addObject = function(obj){
			if (obj.dynamic == true){
			//if (true){
				this.dynamicObjects.push(obj);
			} else {
				this.chunksHolder.addObject(obj);
			}
		}
		
		this.detectCollision = function(obj, endPoint){
			for (var i = 0; i < objects.length; i++){
				if (this.dynamicObjects[i] != obj){
					if (this.detectRectangleCollision(obj, this.dynamicObjects[i], endPoint)){
						return true;
					}
				}
			}
			return false;			
		}
		
		this.detectRectangleCollision = function(obj1, obj2, endPoint){
			if (	endPoint.x < obj2.rectangle.position.x + obj2.rectangle.size.x &&
					endPoint.x + obj1.rectangle.size.x > obj2.rectangle.position.x &&
					endPoint.y < obj2.rectangle.position.y + obj2.rectangle.size.y &&
					obj1.rectangle.size.y + endPoint.y > obj2.rectangle.position.y) {
				//console.log("COLLISION");
				return true;
			}
			return false;
		}
		
		this.detectTouchPoint = function(obj, endPoint){
			var cornerCollisions = [];
			var newRectangle = new window.RPEngine.Rectangle(endPoint.x, endPoint.y, obj.rectangle.size.x, obj.rectangle.size.y);
			//Checking collisions with dynamic objects
			for (var i = 0; i < this.dynamicObjects.length; i++){
				var obj2 = this.dynamicObjects[i];
				if (obj2 != obj){
					if (this.detectRectangleCollision(obj, obj2, endPoint)){
						//ToDo check logic here :
						//Corner collision detection
						
						var lenX = obj2.rectangle.position.x - obj.rectangle.position.x;
						var lenY = obj2.rectangle.position.y - obj.rectangle.position.y;
						if (	((lenX > 0 && lenX == obj.rectangle.size.x) || (lenX < 0 && Math.abs(lenX) == obj2.rectangle.size.x)) &&
								((lenY > 0 && lenY == obj.rectangle.size.y) || (lenY < 0 && Math.abs(lenY) == obj2.rectangle.size.y))){
							cornerCollisions.push(obj2);								
						} else {
							endPoint = this.clacualateTouchPoint(obj, obj2, endPoint);
						}
					}
				}
			}
			//Checking collisions with static objects
			
			var staticObjects = this.chunksHolder.findObjectsInSameChunks(newRectangle);
			for (var i = 0; i < staticObjects.length; i++){
				var obj2 = staticObjects[i];
				//if (obj2.onCamera && obj2 != obj){
				if (obj2 != obj){
					if (this.detectRectangleCollision(obj, obj2, endPoint)){
						//ToDo check logic here :
						//Corner collision detection
						
						var lenX = obj2.rectangle.position.x - obj.rectangle.position.x;
						var lenY = obj2.rectangle.position.y - obj.rectangle.position.y;
						if (	((lenX > 0 && lenX == obj.rectangle.size.x) || (lenX < 0 && Math.abs(lenX) == obj2.rectangle.size.x)) &&
								((lenY > 0 && lenY == obj.rectangle.size.y) || (lenY < 0 && Math.abs(lenY) == obj2.rectangle.size.y))){
							cornerCollisions.push(obj2);								
						} else {
							endPoint = this.clacualateTouchPoint(obj, obj2, endPoint);
						}
					}
				}
			}
			
			for (var i = 0; i < cornerCollisions.length; i++){
				if (this.detectRectangleCollision(obj, cornerCollisions[i], endPoint)){
					endPoint = this.clacualateTouchPoint(obj, cornerCollisions[i], endPoint);
				}
			}
			return endPoint;			
		}
		
		this.clacualateTouchPoint = function(obj1, obj2, endPoint){
			var velX = endPoint.x - obj1.rectangle.position.x;
			var velRealX = velX;
			var velY = endPoint.y - obj1.rectangle.position.y;
			var velRealY = velY;
			var lenX = Math.abs(obj1.rectangle.position.x - obj2.rectangle.position.x);
			var lenY = Math.abs(obj1.rectangle.position.y - obj2.rectangle.position.y);
			if (velX > 0 && lenX >= obj1.rectangle.size.x){
				if (lenX - obj1.rectangle.size.x < velX){
					velRealX = lenX - obj1.rectangle.size.x
				}
			} else if (velX < 0 && lenX >= obj2.rectangle.size.x){
				if (lenX - obj2.rectangle.size.x < -velX){
					velRealX = obj2.rectangle.size.x - lenX
				}
			}
			if (velY > 0 && lenY >= obj1.rectangle.size.y){
				if (lenY - obj1.rectangle.size.y < velY){
					velRealY = lenY - obj1.rectangle.size.y;
				}
			} else if (velY < 0 && lenY >= obj2.rectangle.size.y){
				if (lenY - obj2.rectangle.size.y <= -velY){
					velRealY = obj2.rectangle.size.y - lenY;
				}
			}
			return new window.RPEngine.Point(
				Math.round(obj1.rectangle.position.x + velRealX),
				Math.round(obj1.rectangle.position.y + velRealY)
			);
		}
		
	};
	window.RPEngine.PhysicsEngine.DEFAULT_CONFIG = {
		CHUNK_SIZE: 128
	};
	window.RPEngine.PhysicsEngine.DefaultBody = function(obj){
		this.obj = obj;
		this.layers = [];
	};
	window.RPEngine.PhysicsEngine.RectangletBody = function(obj){
		$.extend(this, new window.RPEngine.PhysicsEngine.DefaultBody(obj));
		this.width = 0;
		this.height = 0;
	};
	window.RPEngine.PhysicsEngine.ChunksHolder = function(sizeX, sizeY, chunkSize){
		this.chunkSize = chunkSize;
		this.sizeX = sizeX;
		this.sizeY = sizeY;
		this.chunks = [];
		
		for (var i = 0; i <= sizeX; i += this.chunkSize){
			var chArr = [];
			this.chunks.push(chArr);
			for (var j = 0; j <= sizeY; j += this.chunkSize){
				chArr.push([]);		
			}	
		}
		
		this.addObject = function(obj){
			var chunks = this.findChunksForRectangle(obj.rectangle);
			obj.chunks = [];
			for (var i = 0; i < chunks.length; i++){
				chunks[i].push(obj);
				obj.chunks.push(chunks[i]);
			}
		}
		
		this.updateObject = function(obj){
			this.deleteObject(obj);
			this.addObject(obj);
		}
		
		this.deleteObject = function(obj){
			for (var i = 0; i < obj.chunks.length; i++){
				var idx = chunks[i].indexOf(obj);
				chunks[i].splice(idx, 1);
			}
			obj.chunks = null;
		}
		
		this.findChunksForRectangle = function(rect){
			var chunks = [];
			var oppositePoint = rect.getOppositePoint();
			var startX = (rect.position.x - rect.position.x % this.chunkSize) / this.chunkSize;
			var startY = (rect.position.y - rect.position.y % this.chunkSize) / this.chunkSize;
			var endX = (oppositePoint.x - oppositePoint.x % this.chunkSize) / this.chunkSize;
			var endY = (oppositePoint.y - oppositePoint.y % this.chunkSize) / this.chunkSize;
			for (var i = startX; i <= endX && i >= 0 && i < this.chunks.length; i++){
				for (var j = startX; j <= endX && j >= 0 && j < this.chunks.length; j++){
					try {
						var ch = this.chunks[i][j];
						chunks.push(ch);
					} catch(ex){
						console.log("ex");
					}
				}
			}
			return chunks;
		}
		
		this.findObjectsInSameChunks = function(rect){
			var chunks = this.findChunksForRectangle(rect);	
			var objects = [];
			for (var i = 0; i < chunks.length; i++){
				if (chunks[i] == undefined){
					console.log(chunks[i].length)
				}
				for (var j = 0; j < chunks[i].length; j++){
					if (objects.indexOf(chunks[i][j]) == -1){
						objects.push(chunks[i][j]);
					}
				}
			}
			return objects;
		}
	}
})();